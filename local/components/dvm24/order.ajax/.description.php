<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
$arComponentDescription = array(
"NAME" => GetMessage("Заказ (Ajax)"),
"DESCRIPTION" => GetMessage("Отправляет данные с формы заказа, на почту покупателю и администратору"),
"PATH" => array(
"ID" => "dvm24_order_ajax",
),
"ICON" => "/images/icon.gif",
);
?>