<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
$arComponentDescription = array(
"NAME" => GetMessage("Категория товаров"),
"DESCRIPTION" => GetMessage("Выводит товары на странице принадлежащие опрделененной категории"),
"PATH" => array(
"ID" => "dvm24_category",
),
"ICON" => "/images/icon.gif",
);
?>