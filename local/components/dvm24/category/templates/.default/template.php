<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>
<div class="featured container no-gutter">
	<div class="row">
		<?
foreach ($arResult['PRODUCTS'] as $arItem) {

	?>
	<div id="<?= $arItem["ID"]?>" class="item new col-md-4">
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
      <a href="<?= $arItem["DETAIL_PAGE_URL"]?>">
        <div class="featured-item">
          <img src="<?= $arItem["IMG_SRC"]?>" alt="">
          <h4><?= $arItem["NAME"]?></h4>
          <h6>$<?= $arItem["PRICE"]?></h6>
        </div>
      </a>
    </div>
	<?
}
?>
	</div>
</div>



