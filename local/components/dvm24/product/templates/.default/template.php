<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
 <div class="single-product">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <div class="line-dec"></div>

              <h1><?= $arResult['NAME']?></h1>
<?
$this->AddEditAction($arResult['ID'], $arResult['EDIT_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arResult['ID'], $arResult['DELETE_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
            </div>
          </div>
          <div class="col-md-6">

<?
$APPLICATION->IncludeComponent(
   "dvm24:product.gallery",
   "",
   [
    'IMAGES_IDS'=>$arResult['PROPERTIES']['GALLERY']['VALUE']
   ],
   false
);
?>
          </div>
          <div class="col-md-6">
            <div class="right-content">
              <h4><?= $arResult['NAME']?></h4>
              <h6>$<?= $arResult['PROPERTIES']['PRICE']['VALUE']?></h6>
              <p><?= $arResult['PROPERTIES']['DESC']['VALUE']['TEXT']?></p>
              <span>
          <?
          $frame = $this->createFrame()->begin("Loading...");
          ?><?= $arResult['PROPERTIES']['QTY']['VALUE']?>
          <?$frame->end();?> left on stock</span>
              <form id="order_form" action="/form/order.php" method="POST">
                <label for="quantity">Quantity:</label>
                <input name="quantity" type="quantity" class="quantity-text" id="quantity" 
                  onfocus="if(this.value == '1') { this.value = ''; }" 
                    onBlur="if(this.value == '') { this.value = '1';}"
                    value="1">
                  <div class="col-md-6" style="margin:20px">
                    <div id="hidden_form" class="right-content" style="display:none">
                      <div class="content">
                    <fieldset>
                        <input name="name" type="text" class="form-control text" id="name" placeholder="Your name..." required="">
                    </fieldset>
                    <fieldset>
                        <input name="phone" type="text" class="form-control text" id="phone" placeholder="Your phone..." required="">
                      </fieldset>
                      <fieldset>
                        <input name="email" type="text" class="form-control text" id="email" placeholder="Your email..." required="">
                      </fieldset>
                      <input type="hidden" name="ID_PRODUCT" value="<?=$arResult['ID']?>">
                    <input id="quick_order" type="submit" class="button" value="Order Now!">
                  </div>
                </div>
                  </div>
                <input id="open_form" type="button" class="button" value="Order Now!">
              </form>
              <div class="down-content">
                <div class="categories">
                  <h6>Category: <span>
                    <?
                    foreach ($arResult['CATEGORIES'] as $category) {
                      ?>
                      <a href="<?=$category["SECTION_PAGE_URL"]?>"><?=$category["NAME"]?></a>,
                      <?
                    }
                    ?>
                  </span></h6>
                </div>
                <div class="share">
                <!--   <h6>Share: <span><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i class="fa fa-twitter"></i></a></span></h6> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>