<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$rsProduct = CIBlockElement::GetList(
	[],
	[
		"=IBLOCK_ID"=>1,
		"ID"=>$_GET['ID']
	],
	false,
	false,
	[
		"ID",
		"NAME",
		"IBLOCK_ID",
		"DETAIL_PAGE_URL"
	]
);

if($obElement = $rsProduct->GetNextElement()){
	$arResult = $obElement->GetFields();
	$APPLICATION->SetTitle($arResult['NAME']);
	$arResult["PROPERTIES"] = $obElement->GetProperties(false,[]);
	$rsCategories =
	CIBlockElement::GetElementGroups(
	 	(int)$_GET['ID'],
	 	false,
	 	[
	 		'ID',
 			'NAME',
 			'SECTION_PAGE_URL'
	 	]
	);
	$arButtons = CIBlock::GetPanelButtons(
		$arResult["IBLOCK_ID"],
		$arResult["ID"],
		0,
		[
			"SECTION_BUTTONS"=>false,
			"SESSID"=>false
		]
	);
	$arResult["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
	$arResult["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
	while ($arCategory = $rsCategories->GetNext())
		$arResult["CATEGORIES"][] = $arCategory;
}
$this->IncludeComponentTemplate();
?>


