<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
$arComponentDescription = array(
"NAME" => GetMessage("Товар"),
"DESCRIPTION" => GetMessage("Выводит информацию о товаре с кнопкой покупки"),
"PATH" => array(
"ID" => "dvm24_product",
// "CHILD" => array(
// "ID" => "curdate",
// "NAME" => "Текущая дата"
// )
),
"ICON" => "/images/icon.gif",
);
?>