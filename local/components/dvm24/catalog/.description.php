<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
$arComponentDescription = array(
"NAME" => GetMessage("Каталог товаров"),
"DESCRIPTION" => GetMessage("Выводит товары на странице с постраничной наигацией"),
"PATH" => array(
"ID" => "dvm24_catalog",
),
"ICON" => "/images/icon.gif",
);
?>