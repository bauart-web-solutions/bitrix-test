<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$nav = new \Bitrix\Main\UI\PageNavigation("catalog");
$nav->allowAllRecords(false)
   ->setPageSize(9)
   ->initFromUri();

$productsList = CIBlockElement::GetList(
	[],
	[
		"=IBLOCK_ID"=>1
	],
	false,
	[
		"nPageSize" => 9,
		"iNumPage"=>$nav->getCurrentPage()
	],
	[
		"ID",
		"NAME",
		"IBLOCK_ID",
		"DETAIL_PAGE_URL",
	]
);
$productsList->NavStart(0);
$nav->setRecordCount($productsList->SelectedRowsCount());
while ($obElement = $productsList->GetNextElement()) {
	$arItem = $obElement->GetFields();
	$arItem["PROPERTIES"] = $obElement->GetProperties(false,[]);
	$fileID = $arItem["PROPERTIES"]["GALLERY"]['VALUE'][0];
	$srcFile = CFile::GetPath($fileID);
	if($srcFile){
		$arItem["IMG_SRC"] = $srcFile;
	}else{
		$arItem["IMG_SRC"] =  SITE_TEMPLATE_PATH."/assets/images/default.png";
	}
	$arItem["PRICE"] = $arItem["PROPERTIES"]["PRICE"]['VALUE'];
	$arButtons = CIBlock::GetPanelButtons(
		$arItem["IBLOCK_ID"],
		$arItem["ID"],
		0,
		[
			"SECTION_BUTTONS"=>false,
			"SESSID"=>false
		]
	);
	$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
	$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
	unset($arItem["PROPERTIES"]);
	$arResult["PRODUCTS"][] = $arItem;
}
$this->IncludeComponentTemplate();
?>


<?
$APPLICATION->IncludeComponent(
   "bitrix:main.pagenavigation",
   ".default",
   array(
      "NAV_OBJECT" => $nav,
      "SEF_MODE" => "Y",
   ),
   false
);
?>
