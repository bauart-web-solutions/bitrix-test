<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
$arComponentDescription = array(
"NAME" => GetMessage("Галерея товара"),
"DESCRIPTION" => GetMessage("Выводим изображения товара в убодной для просмотра форме."),
"PATH" => array(
"ID" => "dvm24_product_gallery",
// "CHILD" => array(
// "ID" => "curdate",
// "NAME" => "Текущая дата"
// )
),
"ICON" => "/images/icon.gif",
);
?>