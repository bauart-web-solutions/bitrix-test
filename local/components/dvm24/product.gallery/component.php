<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arImageIds = $arParams["IMAGES_IDS"];
if(!empty($arImageIds)){
	foreach ($arImageIds as $imageId) {
		$arResult['IMAGES'][] = CFile::GetPath($imageId);
	}
}else{
	$arResult['IMAGES'][] = SITE_TEMPLATE_PATH."/assets/images/default.png";
}
$this->IncludeComponentTemplate();
?>


