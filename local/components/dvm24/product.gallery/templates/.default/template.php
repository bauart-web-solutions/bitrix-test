<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>
<div class="product-slider">
  <div id="slider" class="flexslider">
    <ul class="slides">
      <? foreach($arResult['IMAGES'] as $imgSrc){?>
      <li>
        <img src="<?=$imgSrc?>" />
      </li>
      <?}?>
      
    </ul>
  </div>
  <div id="carousel" class="flexslider">
    <ul class="slides">
      <? foreach($arResult['IMAGES'] as $imgSrc){?>
      <li>
        <img src="<?=$imgSrc?>" />
      </li>
      <?}?>
    </ul>
  </div>
</div>