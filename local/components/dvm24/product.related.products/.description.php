<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
$arComponentDescription = array(
"NAME" => GetMessage("Сопутствующие товары"),
"DESCRIPTION" => GetMessage("Выводит сопутсвующие товары на странице товара"),
"PATH" => array(
"ID" => "dvm24_product_related_products",
// "CHILD" => array(
// "ID" => "curdate",
// "NAME" => "Текущая дата"
// )
),
"ICON" => "/images/icon.gif",
);
?>