<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// ниже код товаров из той же категории, НЕ СОПУТСВУЮЩИХ
// $res = CIBlockElement::GetList(
//  array(
//  'sort' => 'asc'
//  ),
//  array(
//  'IBLOCK_ID' => 1, // здесь ID инфоблока, в котором находится элемент
//  'ACTIVE' => 'Y',
//  'SECTION_ID' => $arResult['IBLOCK_SECTION_ID']
//  ),
//  false,
//  array(
//  'nElementID' => $arResult['ID'],
//  'nPageSize' => 2
//  )
// );
// $nearElementsSide = 'LEFT';
// while ($arElem = $res->GetNext()) {
//  if ($arElem['ID'] == $arResult['ID']) {
//  $nearElementsSide = 'RIGHT';
//  continue;
//  }
//  $arResult['NEAR_ELEMENTS'][$nearElementsSide][] = $arElem;
// }
$rsProduct = CIBlockElement::GetList(
	[],
	[
		"=IBLOCK_ID"=>1,
		"ID"=>$_GET['ID']
	],
	false,
	false,
	[
		"ID",
		"NAME",
		"IBLOCK_ID",
		"DETAIL_PAGE_URL"
	]
);

if($obElement = $rsProduct->GetNextElement()){
	$arResult = $obElement->GetFields();
	$APPLICATION->SetTitle($arResult['NAME']);
	$arResult["PROPERTIES"] = $obElement->GetProperties(false,[]);
	$rsCategories =
	CIBlockElement::GetElementGroups(
	 	(int)$_GET['ID'],
	 	false,
	 	[
	 		'ID',
 			'NAME',
 			'SECTION_PAGE_URL'
	 	]
	);
	while ($arCategory = $rsCategories->GetNext())
		$arResult["CATEGORIES"][] = $arCategory;
}
$this->IncludeComponentTemplate();
?>


