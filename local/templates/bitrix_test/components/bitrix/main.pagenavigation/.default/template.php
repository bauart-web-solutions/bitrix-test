<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
$component = $this->getComponent();

$this->setFrameMode(true);

$colorSchemes = array(
	"green" => "bx-green",
	"yellow" => "bx-yellow",
	"red" => "bx-red",
	"blue" => "bx-blue",
);
if(isset($colorSchemes[$arParams["TEMPLATE_THEME"]]))
{
	$colorScheme = $colorSchemes[$arParams["TEMPLATE_THEME"]];
}
else
{
	$colorScheme = "";
}
?>

<div class="page-navigation <?=$colorScheme?>" style="margin: auto">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
		<ul>
<?if($arResult["REVERSED_PAGES"] === true):?>

	<?if ($arResult["CURRENT_PAGE"] < $arResult["PAGE_COUNT"]):?>
		<?if (($arResult["CURRENT_PAGE"]+1) == $arResult["PAGE_COUNT"]):?>
			<li><a href="<?=htmlspecialcharsbx($arResult["URL"])?>"><i class="fa fa-angle-left"></i></a></li>
		<?else:?>
			<li><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"]+1))?>"><i class="fa fa-angle-left"></i></a></li>
		<?endif?>
			<li ><a href="<?=htmlspecialcharsbx($arResult["URL"])?>">1</a></li>
	<?else:?>
			<li class="disabled"><a><i class="fa fa-angle-left"></i></a></li>
			<li><a>1</a></li>
	<?endif?>

	<?
	$page = $arResult["START_PAGE"] - 1;
	while($page >= $arResult["END_PAGE"] + 1):
	?>
		<?if ($page == $arResult["CURRENT_PAGE"]):?>
			<li class="current-page"><a href="#"><?=($arResult["PAGE_COUNT"] - $page + 1)?></a></li>
		<?else:?>
			<li class=""><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate($page))?>"><?=($arResult["PAGE_COUNT"] - $page + 1)?></a></li>
		<?endif?>

		<?$page--?>
	<?endwhile?>

	<?if ($arResult["CURRENT_PAGE"] > 1):?>
		<?if($arResult["PAGE_COUNT"] > 1):?>
			<li><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate(1))?>"><?=$arResult["PAGE_COUNT"]?></a></li>
		<?endif?>
			<li><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"]-1))?>"><?echo GetMessage("round_nav_forward")?></a></li>
	<?else:?>
		<?if($arResult["PAGE_COUNT"] > 1):?>
			<li><a><?=$arResult["PAGE_COUNT"]?></a></li>
		<?endif?>
			<li><a><?echo GetMessage("round_nav_forward")?></a></li>
	<?endif?>

<?else:?>

	<?if ($arResult["CURRENT_PAGE"] > 1):?>
		<?if ($arResult["CURRENT_PAGE"] > 2):?>
			<li><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"]-1))?>"><i class="fa fa-angle-right"></i></a></li>
		<?else:?>
			<li><a href="<?=htmlspecialcharsbx($arResult["URL"])?>"><i class="fa fa-angle-left"></i></a></li>
		<?endif?>
			<li class=""><a href="<?=htmlspecialcharsbx($arResult["URL"])?>">1</a></li>
	<?else:?>
			<li class="disabled"><a><i class="fa fa-angle-left"></i></a></li>
			<li class="current-page"><a>1</a></li>
	<?endif?>

	<?
	$page = $arResult["START_PAGE"] + 1;
	while($page <= $arResult["END_PAGE"]-1):
	?>
		<?if ($page == $arResult["CURRENT_PAGE"]):?>
			<li class="current-page"><a><?=$page?></a></li>
		<?else:?>
			<li class=""><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate($page))?>"><?=$page?></a></li>
		<?endif?>
		<?$page++?>
	<?endwhile?>

	<?if($arResult["CURRENT_PAGE"] < $arResult["PAGE_COUNT"]):?>
		<?if($arResult["PAGE_COUNT"] > 1):?>
			<li><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate($arResult["PAGE_COUNT"]))?>"><?=$arResult["PAGE_COUNT"]?></a></li>
		<?endif?>
			<li><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"]+1))?>"><i class="fa fa-angle-right"></i></a></li>
	<?else:?>
		<?if($arResult["PAGE_COUNT"] > 1):?>
			<li class="current-page"><a><?=$arResult["PAGE_COUNT"]?></a></li>
		<?endif?>
			<li class="disabled"><a><i class="fa fa-angle-right"></i></a></li>
	<?endif?>
<?endif?>

<?if ($arResult["SHOW_ALL"]):?>
	<?if ($arResult["ALL_RECORDS"]):?>
			<li class="bx-pag-all"><a href="<?=htmlspecialcharsbx($arResult["URL"])?>" rel="nofollow"><?echo GetMessage("round_nav_pages")?></a></li>
	<?else:?>
			<li class="bx-pag-all"><a href="<?=htmlspecialcharsbx($component->replaceUrlTemplate("all"))?>" rel="nofollow"><?echo GetMessage("round_nav_all")?></a></li>
	<?endif?>
<?endif?>
		</ul>
		<div style="clear:both"></div>
	</div>
</div>
	</div>
</div>
