<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	// print_r($arItem);
	// die;
	$res = CFile::GetList([],['@ID' => $arItem["PROPERTIES"]['GALLERY']['VALUE'][0], ]);
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="item new col-md-4">
              <a href="single-product.html">
                <div class="featured-item">
                	<? if($res_arr = $res->GetNext()):?>
                	<? print_r($res_arr);?>
                 	<img src="/upload/<?= $res_arr["SUBDIR"]."/".$res_arr["FILE_NAME"]?>">
                 	<? else:?>
                 	<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/product-02.jpg">
                 	<? endif;?>
                  <h4><?= $arItem["NAME"]?></h4>
                  <h6>$<?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['DISPLAY_VALUE']?></h6>
                </div>
              </a>
            </div>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
