<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
global $APPLICATION;
use \Bitrix\Main\Page\Asset;
// include_once('meta.php');
?>
<!doctype html>
<html lang="ru">
	<head>
				<? Asset::getInstance()->addJs( SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.min.js');?>
		<? Asset::getInstance()->addJs( SITE_TEMPLATE_PATH . '/vendor/bootstrap/js/bootstrap.bundle.min.js');?>
		<? Asset::getInstance()->addJs( SITE_TEMPLATE_PATH . '/assets/js/custom.js');?>
		<? Asset::getInstance()->addJs( SITE_TEMPLATE_PATH . '/assets/js/owl.js');?>
		<? Asset::getInstance()->addJs( SITE_TEMPLATE_PATH . '/assets/js/isotope.js');?>
		<? Asset::getInstance()->addJs( SITE_TEMPLATE_PATH . '/assets/js/flex-slider.js');?>

		<? Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700");?>
		<? Asset::getInstance()->addCss( SITE_TEMPLATE_PATH . '/vendor/bootstrap/css/bootstrap.min.css');?>
		<? Asset::getInstance()->addCss( SITE_TEMPLATE_PATH . '/assets/css/fontawesome.css');?>
		<? Asset::getInstance()->addCss( SITE_TEMPLATE_PATH . '/assets/css/tooplate-main.css');?>
		<? Asset::getInstance()->addCss( SITE_TEMPLATE_PATH . '/assets/css/owl.css');?>
		<? Asset::getInstance()->addCss( SITE_TEMPLATE_PATH . '/assets/css/flex-slider.css');?>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle()?></title>
	</head>
	<body>
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div id="pre-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- <span>Suspendisse laoreet magna vel diam lobortis imperdiet</span> -->
          </div>
        </div>
      </div>
    </div>
   
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	".default", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPOSITE_FRAME_MODE" => "Y",
		"COMPOSITE_FRAME_TYPE" => "DYNAMIC_WITH_STUB_LOADING"
	),
	false
);?>
