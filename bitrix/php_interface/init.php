<?
require_once("FirePHPCore/fb.php");
AddEventHandler('main', 'OnCheckListGet', array('CItcCheckListTests', 'onCheckListGet'));

class CItcCheckListTests
{
    static public function onCheckListGet($arCheckList)
    {
    	$arCheckList=[];
        $checkList = array('CATEGORIES' => array(), 'POINTS' => array());

        $checkList['CATEGORIES']['ITC_QC'] = array(
            'NAME' => '������������� ���� �������� ITConstruct',
            'LINKS' => ''
        );

        $checkList['POINTS']['ITC_QC_FAVICON'] = array(
            'PARENT' => 'ITC_QC',
            'REQUIRE' => 'Y',
            'AUTO' => 'Y',
            'CLASS_NAME' => __CLASS__,
            'METHOD_NAME' => 'checkFavicon',
            'NAME' => '������� favicon',
            'DESC' => '�������� ������� favicon - ������ �����, ������������ � ��������� ������� � ��������� ��������',
            'HOWTO' => '������������ �������� ������� �������� ����� �� ������� ���������������� ����-����. ���� ��� �������� - ����������� ������� ������ �� ���������� ����. ���� �� ������ - ������� favicon.ico � ����� �����',
            'LINKS' => 'links'
        );
        $checkList['POINTS']['ITC_QC_DEFAULT_PRODUCT_IMAGE'] = array(
            'PARENT' => 'ITC_QC',
            'REQUIRE' => 'Y',
            'AUTO' => 'Y',
            'CLASS_NAME' => __CLASS__,
            'METHOD_NAME' => 'checkDefaultProductImage',
            'NAME' => '������� ������������ ����������� ��� ��������',
            'DESC' => '�������� ������� ����������� ������������� � ������� ��� �����������',
            'HOWTO' => '����������� ������� ����� � ����� ������� �����',
            'LINKS' => 'links'
        );

        $checkList['POINTS']['ITC_QC_DENY_DEV'] = array(
            'PARENT' => 'ITC_QC',
            'REQUIRE' => 'N',
            'AUTO' => 'N',
            'NAME' => '�������� ������� ����� � dev-�������',
            'DESC' => '����������� � ���������� �������� ������� �� ����������� ������� ���������� �� �������� ����',
            'HOWTO' => '����������� � �������� ����� ������� ����',
        );

        return $checkList;
    }

    static public function checkFavicon($arParams)
    {
        $arResult = array('STATUS' => 'F');
        $check = file_exists($_SERVER['DOCUMENT_ROOT'] . '/favicon.ico');

        if ($check === true) {
            $arResult = array(
                'STATUS' => true,
                'MESSAGE' => array(
                    'PREVIEW' => 'Favicon ������� - ' . '/favicon.ico',
                ),
            );
        } else {
            $arResult = array(
                'STATUS' => false,
                'MESSAGE' => array(
                    'PREVIEW' => 'Favicon �� �������',
                    'DETAIL' => '���� ����� ��������, �� ��� � �� ���� ����� ���������. �� � ���� � ���',
                ),
            );
        }

        return $arResult;
    }
    static public function checkDefaultProductImage($arParams)
    {
        $arResult = array('STATUS' => 'F');
        $filePath = $_SERVER['DOCUMENT_ROOT']."/local/templates/bitrix_test/assets/images/default.png";
        // fb($filePath);
        $check = file_exists($filePath);

        if ($check === true) {
            $arResult = array(
                'STATUS' => true,
                'MESSAGE' => array(
                    'PREVIEW' => '���� �������� ����������� ������������',
                ),
            );
        } else {
            $arResult = array(
                'STATUS' => false,
                'MESSAGE' => array(
                    'PREVIEW' => '����  �������� ����������� ����������',
                ),
            );
        }

        return $arResult;
    }
}