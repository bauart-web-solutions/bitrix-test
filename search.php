<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Search");
?>
<div class="featured-page">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="section-heading">
					<div class="line-dec">
					</div>
					<h1>Search</h1>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
<?$APPLICATION->IncludeComponent(
	"bitrix:search.page",
	"",
Array()
);?>
			</div>

		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>