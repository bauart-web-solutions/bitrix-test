<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Catalog");
?>
<div class="featured-page">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="section-heading">
					<div class="line-dec">
					</div>
					<h1>Featured Items</h1>
				</div>
			</div>
			<div class="col-md-8 col-sm-12">
				<div id="filters" class="button-group">
 <button class="btn btn-primary" data-filter="*">All Products</button> <button class="btn btn-primary" data-filter=".new">Newest</button> <button class="btn btn-primary" data-filter=".low">Low Price</button> <button class="btn btn-primary" data-filter=".high">Hight Price</button>
				</div>
			</div>
			<?$APPLICATION->IncludeComponent(
   "dvm24:catalog",
   "",
   array(),
   false
);?>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>